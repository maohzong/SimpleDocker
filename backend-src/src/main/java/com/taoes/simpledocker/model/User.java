package com.taoes.simpledocker.model;

import lombok.Data;

/**
 * 用户对象
 *
 * @author 枕上江南 zhoutao925638@vip.qq.com
 * @date 2021/12/15 1:26 下午
 */
@Data
public class User {
}
